package desafio_3;

public enum Naipe {
    C(1),
    O(2),
    P(3),
    E(4);

    public final int valor;

    Naipe(int valor) {
        this.valor = valor;
    }
}
