package desafio_2;

import java.util.Random;

public class Constelacoes {
    public Random geradorDeAleatorio;
    public int[][] matrizDeConstelacoes;
    public int x;
    public int y;
    public int z;

    public void procurarInterligacao(String entrada){
        if(entrada.charAt(1) - '0' < 4 || entrada.charAt(1) - '0' > 8){
            throw new IllegalArgumentException("Tamanho da matriz deve ser 4 e 8");
        }

        this.x = entrada.charAt(1) - 48;
        this.y = entrada.charAt(4) - 48;
        this.z = entrada.charAt(7) - 48;
        this.matrizDeConstelacoes = new int[this.x][this.x];
        this.geradorDeAleatorio = new Random();

        gerarMatriz();
        procurarInterligacao();
    }

    private void gerarMatriz(){
        for (int i = 0; i < this.x; i++) {
            for (int j = 0; j < this.x; j++) {
                this.matrizDeConstelacoes[i][j] = this.geradorDeAleatorio.nextInt(2);
            }
        }
    }

    private void imprimirMatriz(){
        System.out.println("[\n");
        for (int i = 0; i < this.x; i++) {
            System.out.print("  [  ");
            for (int j = 0; j < this.x; j++) {
                System.out.print(this.matrizDeConstelacoes[i][j] + "  ");
            }
            System.out.println("]\n");
        }
        System.out.print("], ");
    }

    public void procurarInterligacao(){
        for(int i = 0; i < this.x; i++){
            for(int j = 0; j < this.x; j++){
                if(this.matrizDeConstelacoes[this.y][this.z] == 1){
                    imprimirMatriz();
                    System.out.println("Existe ligação");
                    return;
                }
            }
        }
        imprimirMatriz();
        System.out.println("Não há ligação");
    }


}
