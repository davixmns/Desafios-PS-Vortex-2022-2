package desafio_3;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Dealer dealer = new Dealer();
        Robo robo1 = new Robo();
        Robo robo2 = new Robo();

        ArrayList<Carta> mesa = dealer.darCartas(5);
        robo1.receberCartas(dealer.darCartas(2));
        robo2.receberCartas(dealer.darCartas(2));

        System.out.print("Robô 1 = ");
        robo1.descobrirMelhorMao(mesa);

        System.out.print("Robô 2 = ");
        robo2.descobrirMelhorMao(mesa);

        if (robo1.pontuacao > robo2.pontuacao) {
            System.out.println("Robo 1 venceu!");
        } else if (robo1.pontuacao < robo2.pontuacao) {
            System.out.println("Robo 2 venceu!");
        } else {
            System.out.println("Empate!");
        }
    }
}

