package desafio_3;

public class Carta {
    public int valor;
    public Enum<?> naipe;

    public Carta(int valor, Enum<?> naipe) {
        this.valor = valor;
        this.naipe = naipe;
    }
}
