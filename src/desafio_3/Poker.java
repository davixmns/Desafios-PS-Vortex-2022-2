package desafio_3;

import java.util.ArrayList;

public class Poker {
    public ArrayList<Carta> removerRepetivos(ArrayList<Carta> cartas, Integer valor) {
        cartas.removeIf(c -> c.valor == valor);
        return cartas;
    }

    private boolean verificarSeNaipesSaoIguais(ArrayList<Carta> cartas) {
        return verificarSeTodosSaoCopas(cartas) || verificarSeTodosSaoOuros(cartas) || verificarSeTodosSaoPaus(cartas) || verificarSeTodosSaoEspadas(cartas);
    }

    public boolean par(ArrayList<Carta> cartas) {
        int contador = 0;
        for (int i = 0; i < cartas.size(); i++) {
            for (int j = i + 1; j < cartas.size(); j++) {
                if (cartas.get(i).valor == cartas.get(j).valor) {
                    contador++;
                }
            }
        }
        return contador == 1;
    }

    public boolean doisPares(ArrayList<Carta> cartas) {
        int valorRepetido = 0;
        int contador = 0;
        for (int i = 0; i < cartas.size(); i++) {
            for (int j = i + 1; j < cartas.size(); j++) {
                if (cartas.get(i).valor == cartas.get(j).valor) {
                    contador++;
                    valorRepetido = cartas.get(i).valor;
                }
            }
        }
        if (contador == 2) {
            cartas = removerRepetivos(cartas, valorRepetido);

            return cartas.get(0).valor == cartas.get(1).valor || cartas.get(0).valor == cartas.get(2).valor;
        }
        return false;
    }

    public boolean trio(ArrayList<Carta> cartas) {
        int cartaRepetida = 0;
        int contador = 0;
        for (int i = 0; i < cartas.size(); i++) {
            for (int j = i + 1; j < cartas.size(); j++) {
                if (cartas.get(i).valor == cartas.get(j).valor) {
                    contador++;
                    cartaRepetida = cartas.get(i).valor;
                }
            }
        }
        if (contador == 3) {
            removerRepetivos(cartas, cartaRepetida);
            return cartas.get(0) != cartas.get(1);
        }
        return false;
    }

    public boolean straight(ArrayList<Carta> cartas) {
        if (verificarSeNaipesSaoIguais(cartas)) {
            return !royalFlush(cartas) && !straightFlush(cartas) && !flush(cartas);
        }
        return false;
    }

    public boolean flush(ArrayList<Carta> cartas) {
        if (verificarSeNaipesSaoIguais(cartas)) {
            return !straightFlush(cartas) && !royalFlush(cartas);
        }
        return false;
    }

    public boolean fullHouse(ArrayList<Carta> cartas) {
        int contador = 1;
        for (int i = 0; i < cartas.size(); i++) {
            for (int j = i + 1; j < cartas.size(); j++) {
                if (cartas.get(i).valor == cartas.get(j).valor) {
                    contador++;
                }
            }
        }

        return contador == 5;
    }

    public boolean quadra(ArrayList<Carta> cartas) {
        int contador = 0;
        for (int i = 0; i < cartas.size(); i++) {
            for (Carta carta : cartas) {
                if (cartas.get(i).valor == carta.valor) {
                    contador++;
                }
            }
        }
        return contador == 17;
    }

    public boolean straightFlush(ArrayList<Carta> cartas) {
        ArrayList<Integer> numeros = new ArrayList<>(cartas.stream().map(carta -> carta.valor).collect(ArrayList::new, ArrayList::add, ArrayList::addAll));

        if (numeros.contains(14)) {
            return false;

        } else {
            if (verificarSeNaipesSaoIguais(cartas)) {
                numeros.sort(Integer::compareTo);
                for (int i = 0; i < numeros.size() - 1; i++) {
                    if (numeros.get(i) + 1 != numeros.get(i + 1)) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }


    public boolean royalFlush(ArrayList<Carta> cartas) {
        if (verificarSeNaipesSaoIguais(cartas)) {
            ArrayList<Integer> numeros = new ArrayList<>(cartas.stream().map(carta -> carta.valor).collect(ArrayList::new, ArrayList::add, ArrayList::addAll));

            return numeros.contains(10) && numeros.contains(11) && numeros.contains(12) && numeros.contains(13) && numeros.contains(14);
        }
        return false;
    }

    public boolean verificarSeTodosSaoEspadas(ArrayList<Carta> cartas) {
        boolean flag = true;
        for (Carta carta : cartas) {
            if (!carta.naipe.equals(Naipe.E)) {
                flag = false;
                break;
            }
        }
        return flag;
    }

    public boolean verificarSeTodosSaoOuros(ArrayList<Carta> cartas) {
        boolean flag = true;
        for (Carta carta : cartas) {
            if (!carta.naipe.equals(Naipe.O)) {
                flag = false;
                break;
            }
        }
        return flag;
    }

    public boolean verificarSeTodosSaoPaus(ArrayList<Carta> cartas) {
        boolean flag = true;
        for (Carta carta : cartas) {
            if (!carta.naipe.equals(Naipe.P)) {
                flag = false;
                break;
            }
        }
        return flag;
    }

    public boolean verificarSeTodosSaoCopas(ArrayList<Carta> cartas) {
        boolean flag = true;
        for (Carta carta : cartas) {
            if (!carta.naipe.equals(Naipe.C)) {
                flag = false;
                break;
            }
        }
        return flag;
    }
}
