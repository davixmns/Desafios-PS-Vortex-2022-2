package desafio_1;

import java.util.ArrayList;

public class TradutorRomano {
    private final ArrayList<Integer> listaDeNumerosTraduzidos;
    private int numeroTraduzido;

    public TradutorRomano() {
        this.listaDeNumerosTraduzidos = new ArrayList<>();
        this.numeroTraduzido = 0;
    }

    public Integer traduzirCaractere(char caractere) {
        int numero = 0;
        switch (caractere) {
            case 'I' -> numero = 1;
            case 'V' -> numero = 5;
            case 'X' -> numero = 10;
            case 'L' -> numero = 50;
            case 'C' -> numero = 100;
            case 'D' -> numero = 500;
            case 'M' -> numero = 1000;
        }
        return numero;
    }

    public Integer traduzir(String texto) {
        if (texto.length() == 1) {
            return traduzirCaractere(texto.charAt(0));

        } else {
            for (int i = 0; i < texto.length(); i++) {
                listaDeNumerosTraduzidos.add(traduzirCaractere(texto.charAt(i)));
            }

            boolean flag = false;
            int anterior = listaDeNumerosTraduzidos.get(0);
            for (int j = 1; j < listaDeNumerosTraduzidos.size(); j++) {
                int atual = listaDeNumerosTraduzidos.get(j);

                if (atual > anterior) {
                    if (j == listaDeNumerosTraduzidos.size() - 1) {
                        this.numeroTraduzido += atual - anterior;
                        return this.numeroTraduzido;

                    } else {
                        numeroTraduzido += (atual - anterior);
                        flag = true;
                    }

                } else {
                    if (j == listaDeNumerosTraduzidos.size() - 1 && flag) {
                        this.numeroTraduzido += atual;
                        return this.numeroTraduzido;

                    } else if (j == listaDeNumerosTraduzidos.size() - 1 && !flag) {
                        this.numeroTraduzido += atual + anterior;
                        return this.numeroTraduzido;

                    } else {
                        numeroTraduzido += anterior;
                        flag = false;
                    }
                }
                anterior = atual;
            }
        }

        return numeroTraduzido;
    }
}
