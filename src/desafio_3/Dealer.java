package desafio_3;

import java.util.ArrayList;
import java.util.Random;

public class Dealer {
    private final ArrayList<Carta> cartas;
    private final Random random = new Random();

    public Dealer(){
        this.cartas = new ArrayList<>();

        for(int i = 2; i <= 14; i++){
            this.cartas.add(new Carta(i, Naipe.C));
            this.cartas.add(new Carta(i, Naipe.O));
            this.cartas.add(new Carta(i, Naipe.P));
            this.cartas.add(new Carta(i, Naipe.E));
        }
    }

    public ArrayList<Carta> darCartas(int numero){
        ArrayList<Carta> cartasSorteadas = new ArrayList<>();
        for (int i = 0; i < numero; i++) {
            cartasSorteadas.add(this.cartas.get(this.random.nextInt(this.cartas.size())));
        }
        this.cartas.removeAll(cartasSorteadas);
        return cartasSorteadas;
    }
}
