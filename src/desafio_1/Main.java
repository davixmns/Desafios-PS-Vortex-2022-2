package desafio_1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        TradutorRomano tradutorRomano = new TradutorRomano();

        while (true) {
            System.out.println("Digite um número romano: ");
            System.out.println(tradutorRomano.traduzir(scanner.nextLine()));
            tradutorRomano = new TradutorRomano();
        }
    }
}
