package desafio_3;

import java.util.ArrayList;
import java.util.Collections;

public class Robo {
    private final ArrayList<String> listaDePossibilidades;
    public ArrayList<Carta> cartasNaMao;
    private final Poker poker;
    public int pontuacao;

    public Robo() {
        this.pontuacao = 0;
        this.cartasNaMao = new ArrayList<>();
        this.poker = new Poker();
        this.listaDePossibilidades = new ArrayList<>();
    }

    public void receberCartas(ArrayList<Carta> cartas) {
        this.cartasNaMao.addAll(cartas);
    }

    public void descobrirMelhorMao(ArrayList<Carta> cartasDaMesa){
        for (int i = 0; i < 20; i++) {
            ArrayList<Carta> cartas = new ArrayList<>(this.cartasNaMao);

            cartas.add(cartasDaMesa.get(0));
            cartas.add(cartasDaMesa.get(1));
            cartas.add(cartasDaMesa.get(2));

            descobrirMaoAtual(cartas);
            Collections.shuffle(cartasDaMesa);
        }

        printarMelhorMao();
    }

    public void descobrirMaoAtual(ArrayList<Carta> cartas) {
        StringBuilder maoCartas = new StringBuilder();

        for (Carta c: cartas) {
            maoCartas.append(" ").append(c.valor).append("-").append(c.naipe).append(" ");
        }

        if(poker.royalFlush(cartas)){
            this.listaDePossibilidades.add("Royal Flush");
            this.listaDePossibilidades.add(maoCartas.toString());

        } else if(poker.straightFlush(cartas)){
            this.listaDePossibilidades.add("Straight Flush");
            this.listaDePossibilidades.add(maoCartas.toString());

        } else if(poker.quadra(cartas)){
            this.listaDePossibilidades.add("Quadra");
            this.listaDePossibilidades.add(maoCartas.toString());

        } else if(poker.fullHouse(cartas)){
            this.listaDePossibilidades.add("Full House");
            this.listaDePossibilidades.add(maoCartas.toString());

        } else if(poker.flush(cartas)){
            this.listaDePossibilidades.add("Flush");
            this.listaDePossibilidades.add(maoCartas.toString());

        } else if(poker.straight(cartas)){
            this.listaDePossibilidades.add("Straight");
            this.listaDePossibilidades.add(maoCartas.toString());

        } else if(poker.trio(cartas)){
            this.listaDePossibilidades.add("Trio");
            this.listaDePossibilidades.add(maoCartas.toString());

        } else if(poker.doisPares(cartas)){
            this.listaDePossibilidades.add("Dois Pares");
            this.listaDePossibilidades.add(maoCartas.toString());

        } else if(poker.par(cartas)){
            this.listaDePossibilidades.add("Par");
            this.listaDePossibilidades.add(maoCartas.toString());

        } else {
            this.listaDePossibilidades.add("Carta Alta");
            this.listaDePossibilidades.add(maoCartas.toString());
        }
    }


    public void printarMelhorMao(){
        if(this.listaDePossibilidades.contains("Royal Flush")){
            int localDaMaoNaLista = this.listaDePossibilidades.indexOf("Royal Flush") + 1;
            System.out.println("Royal Flush (" + this.listaDePossibilidades.get(localDaMaoNaLista) + ")");
            this.pontuacao = 10;

        } else if(this.listaDePossibilidades.contains("Straight Flush")){
            int localDaMaoNaLista = this.listaDePossibilidades.indexOf("Straight Flush") + 1;
            System.out.println("Straight Flush (" + this.listaDePossibilidades.get(localDaMaoNaLista) + ")");
            this.pontuacao = 9;

        } else if(this.listaDePossibilidades.contains("Quadra")){
            int localDaMaoNaLista = this.listaDePossibilidades.indexOf("Quadra") + 1;
            System.out.println("Quadra (" + this.listaDePossibilidades.get(localDaMaoNaLista) + ")");
            this.pontuacao = 8;

        } else if(this.listaDePossibilidades.contains("Full House")){
            int localDaMaoNaLista = this.listaDePossibilidades.indexOf("Full House") + 1;
            System.out.println("Full House (" + this.listaDePossibilidades.get(localDaMaoNaLista) + ")");
            this.pontuacao = 7;

        } else if(this.listaDePossibilidades.contains("Flush")){
            int localDaMaoNaLista = this.listaDePossibilidades.indexOf("Flush") + 1;
            System.out.println("Flush (" + this.listaDePossibilidades.get(localDaMaoNaLista) + ")");
            this.pontuacao = 6;

        } else if(this.listaDePossibilidades.contains("Straight")){
            int localDaMaoNaLista = this.listaDePossibilidades.indexOf("Straight") + 1;
            System.out.println("Straight (" + this.listaDePossibilidades.get(localDaMaoNaLista) + ")");
            this.pontuacao = 5;

        } else if(this.listaDePossibilidades.contains("Trio")){
            int localDaMaoNaLista = this.listaDePossibilidades.indexOf("Trio") + 1;
            System.out.println("Trio (" + this.listaDePossibilidades.get(localDaMaoNaLista) + ")");
            this.pontuacao = 4;

        } else if(this.listaDePossibilidades.contains("Dois Pares")){
            int localDaMaoNaLista = this.listaDePossibilidades.indexOf("Dois Pares") + 1;
            System.out.println("Dois Pares (" + this.listaDePossibilidades.get(localDaMaoNaLista) + ")");
            this.pontuacao = 3;

        } else if(this.listaDePossibilidades.contains("Par")){
            int localDaMaoNaLista = this.listaDePossibilidades.indexOf("Par") + 1;
            System.out.println("Par (" + this.listaDePossibilidades.get(localDaMaoNaLista) + ")");
            this.pontuacao = 2;

        } else {
            int localDaMaoNaLista = this.listaDePossibilidades.indexOf("Carta Alta") + 1;
            System.out.println("Carta Alta (" + this.listaDePossibilidades.get(localDaMaoNaLista) + ")");
            this.pontuacao = 1;

        }
    }
}
